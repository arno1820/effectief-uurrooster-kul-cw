const htmlparser2 = require('htmlparser2');
const luxon = require('luxon');

/**************************/
/*** SCRAPING & PARSING ***/
/**************************/

module.exports = {
    scrapeCalendarInfo: function(semesters) {
        let calendarEntries = [];

        let inDate = false;
        let curDate = null;
        let inTable = false;
        let inTableRow = false;
        let inTableCell = false;
        let curTableCell = 0;
        let inCourseName = false;
        let calendarTableEntry = { course: {} };
        const parser = new htmlparser2.Parser(
            {
                onopentagname(name) {
                    if (name == "i") {
                        inDate = true;
                    }
                    else if (name == "table" && curDate != null) {
                        inTable = true;
                    }
                    else if (name == "tr" && inTable) {
                        inTableRow = true;
                    }
                    else if (name == "td" && inTableRow) {
                        inTableCell = true;
                    }
                },
                onopentag(name, attrs) {
                    if (name == "a" && inTableCell) {
                        inCourseName = true;
                        calendarTableEntry.course.link = attrs["href"]
                    }
                },
                ontext(text) {
                    if (inDate) {
                        curDate = parseDate(text);
                    }
                    else if (inTableCell) {
                        switch (curTableCell) {
                            case 0:
                                calendarTableEntry.timespan = parseEntryTimespan(curDate, text);
                                break;
                            case 2:
                                calendarTableEntry.location = parseEntryLocation(text);
                                break;
                            case 4:
                                if (inCourseName) {
                                    calendarTableEntry.course.name = parseEntryCourse(text);
                                }
                                break;
                        }
                    }
                },
                onclosetag(name) {
                    if (name == "i") {
                        inDate = false;
                    }
                    else if (name == "table") {
                        curDate = null;
                        inTable = false;
                    }
                    else if (name == "tr") {
                        calendarEntries.push(calendarTableEntry);
                        calendarTableEntry = { course: {} };
                        curTableCell = 0;
                        inTableRow = false;
                    }
                    else if (name == "td") {
                        curTableCell++;
                        inTableCell = false;
                    }
                    else if (name == "a" && inTableCell) {
                        inCourseName = false;
                        inTableCell = false;
                    }
                }
            },
            {
                decodeEntities: false
            }
        )

        for (let semester of semesters) {
            parser.write(semester);
            parser.end();
        }

        return calendarEntries;
    }
}

function parseDate(text) {
    if (text == null) {
        return null;
    }

    let dateParts = text.split(" ");
    if (dateParts.length == 0) {
        return null;
    }

    let numericDateWithColon = dateParts[dateParts.length - 1];
    if (numericDateWithColon.length != 11) {
        return null;
    }

    let numericDate = numericDateWithColon.substring(0, 10);
    dateParts = numericDate.split(".");
    if (dateParts.length != 3) {
        return null;
    }

    return luxon.DateTime.fromObject({ year: dateParts[2] / 1, month: dateParts[1] / 1, day: dateParts[0] / 1, zone: 'Europe/Brussels' });
}

function parseEntryTimespan(date, text) {
    let spanParts = text.split('&#8212;');
    return {
        start: parseTimespan(date, spanParts[0]),
        end: parseTimespan(date, spanParts[1])
    };
}

function parseTimespan(date, time) {
    let parts = time.split(":");
    return date.plus({ hours: parts[0] / 1, minutes: parts[1] / 1 });
}

function parseEntryLocation(elem) {
    return elem.replace(/\s\s+/g, ' ');
}

function parseEntryCourse(text) {
    return text;
}