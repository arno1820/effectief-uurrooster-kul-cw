const scraper = require("./scraperAndParser");
const process = require("process");
const luxon = require("luxon");

/*****************/
/*** FILTERING ***/
/*****************/

function generateFilteredSet(calendarEntries, filter) {
  let courses = new Set();
  for (let i = 0; i < calendarEntries.length; i++) {
    if (filter.isBase64) {
      if (filter.includes(ntob(calendarEntries[i].course.name.hashCode()))) {
        courses.add(calendarEntries[i]);
      }
    } else {
      if (filter.includes(calendarEntries[i].course.name.hashCode())) {
        courses.add(calendarEntries[i]);
      }
    }
  }

  return courses;
}

/***********************/
/*** ICAL GENERATION ***/
/***********************/

function generateICal(calendarInfo) {
  let data = "";
  data += "BEGIN:VCALENDAR\r\n";
  data += "VERSION:2.0\r\n";
  data +=
    "PRODID:-//cweu.herokuapp.com/Generator Effectief Uurrooster CW KULeuven//NL\r\n";
  for (event of calendarInfo) {
    data += generateICalEvent(event);
  }
  data += "END:VCALENDAR\r\n";
  return data;
}

function padInt(val, minLength) {
  let str = "" + val;
  while (str.length < minLength) {
    str = "0" + str;
  }
  return str;
}

function generateICalDateTimeString(datetime) {
  let v = datetime.setZone("utc"); //.toFormat("yyyyMMdd'T'HHmmss'Z'");
  return `${v.year}${padInt(v.month, 2)}${padInt(v.day, 2)}T${padInt(
    v.hour,
    2
  )}${padInt(v.minute, 2)}${padInt(v.second, 2)}Z`;
}

function generateICalEvent(event) {
  let dtstamp = generateICalDateTimeString(
    luxon.DateTime.local().setZone("Europe/Brussels")
  );
  let e = "";
  e += "BEGIN:VEVENT\r\n";
  e +=
    "UID:" +
    dtstamp +
    "-" +
    process.pid +
    "-" +
    Math.floor(Math.random() * 999999) +
    "@cweu.herokuapp.com\r\n";
  e += "SUMMARY:" + event.course.name + "\r\n";
  e += "DTSTAMP:" + dtstamp + "\r\n";
  e += "DTSTART:" + generateICalDateTimeString(event.timespan.start) + "\r\n";
  e += "DTEND:" + generateICalDateTimeString(event.timespan.end) + "\r\n";
  e += "URL:" + event.course.link + "\r\n";
  e += "LOCATION:" + event.location + "\r\n";
  e += "END:VEVENT\r\n";
  return e;
}

/*** MISC ***/

// Source: https://stackoverflow.com/questions/7616461/generate-a-hash-from-string-in-javascript
String.prototype.hashCode = function() {
  var hash = 0,
    i,
    chr;
  if (this.length === 0) return hash;
  for (i = 0; i < this.length; i++) {
    chr = this.charCodeAt(i);
    hash = (hash << 5) - hash + chr;
    hash |= 0; // Convert to 32bit integer
  }
  return hash;
};

// number to base64
// Source: https://github.com/kutuluk/number-to-base64/
const ntob = number => {
  let alphabet =
    "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789-_";
  // binary to string lookup table
  let b2s = alphabet.split("");

  if (number < 0) return `-${ntob(-number)}`;

  let lo = number >>> 0;
  let hi = (number / 4294967296) >>> 0;

  let right = "";
  while (hi > 0) {
    right = b2s[0x3f & lo] + right;
    lo >>>= 6;
    lo |= (0x3f & hi) << 26;
    hi >>>= 6;
  }

  let left = "";
  do {
    left = b2s[0x3f & lo] + left;
    lo >>>= 6;
  } while (lo > 0);

  return left + right;
};

module.exports = {
  loadCalendarInfo: function(bodySemester1, bodySemester2, filter) {
    let calendarEntries = scraper.scrapeCalendarInfo([
      bodySemester1,
      bodySemester2
    ]);

    let cal = generateFilteredSet(calendarEntries, filter);

    let icalData = generateICal(cal);
    return icalData;
  }
};
