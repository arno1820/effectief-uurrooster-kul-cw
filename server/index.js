const express = require("express");
const request = require("request");
const PORT = process.env.PORT || 8080;
const app = express();
const generateIcs = require("./generateIcs");
const Bourne = require("secure-json-parse");
const scraper = require("./scraperAndParser");
const cache = require("memory-cache");
let statistics = [];

function getSchedule(callback) {
  let date = new Date();
  let startYear = date.getFullYear();
  let endYear = date.getFullYear() + 1;
  // if true => second half of academic year.
  if (date.getMonth() < 8) {
    startYear -= 1;
    endYear -= 1;
  }
  let academicYear =
    startYear.toString().slice(-2) + endYear.toString().slice(-2);

  // console.log('Current month: %d', date.getMonth())
  // console.log('Results in this info about the academic year: startYear %d, endYear %d, academicYear %s', startYear, endYear, academicYear);

  // use a timeout value of 10 seconds
  const timeoutInMilliseconds = 10 * 1000;
  let optsSemester1 = {
    url: `https://people.cs.kuleuven.be/~btw/roosters${academicYear}/cws_semester_1.html`,
    timeout: timeoutInMilliseconds
  };
  let optsSemester2 = {
    url: `https://people.cs.kuleuven.be/~btw/roosters${academicYear}/cws_semester_2.html`,
    timeout: timeoutInMilliseconds
  };

  request(optsSemester1, function(error, result, bodySemester1) {
    if (error) {
      console.dir(error);
      return;
    } else if (result.statusCode != 200) {
      console.error("Page " + optsSemester1.url + " returned status code " + result.statusCode);
      console.dir(result);
    } else {
      request(optsSemester2, function(error, result, bodySemester2) {
        if (error) {
          console.dir(error);
          return;
        } else if (result.statusCode != 200) {
          console.error("Page " + optsSemester2.url + " returned status code " + result.statusCode);
          console.dir(result);
        } else {
          callback(bodySemester1, bodySemester2);
        }
      });
    }
  });
}

function monitor(req) {
  let found = false;
  let url = req.originalUrl;
  for (let i = 0; i < statistics.length; i++) {
    let statistic = statistics[i];
    if (statistic.url === url) {
      // Reset time of request.
      statistic.time = Date.now();
      found = true
    } else if (statistic.time < Date.now() - 7 * 24 * 60 * 60 * 1000) {
      // Remove request if older than 7 days.
      statistics.splice(i, 1);
      i--;
    }
  }
  if (!found) {
    statistics.push({
      url: url,
      time: Date.now()
    });
  }

  console.log("Unique requests in the last 7 days: " + statistics.length);

  let averages = cache.get('averages');
  let lastTime = 0;

  if (!averages) {
    averages = [];
  } else {
    lastTime = averages[0].uptime;
  }

  if (averages.length >= 1000) {
    averages.pop();
  }

  let uptime = process.uptime();
  averages.unshift({
    uptime: uptime,
    rps: 1 / (uptime - lastTime)
  });

  console.log(
    "latency: "
    + Math.round((uptime - lastTime) * 100) / 100
    + " rps: "
    + averages.reduce((acc, cur, index) => index <= 50 ? acc + cur.rps : acc, 0)
      / averages.length
  );

  cache.put('averages', averages);
}

app.get("/", (req, res) => {
  // Backwards compatibility of the get parameter vakken.
  if (!req.query.c && !req.query.courses && !req.query.vakken) {
    res.redirect("/generator");
    return;
  }

  if (req.query.c) {
    // Adding quotes to make get param json legal.
    // Added here instead of during generation to get shorter links.
    let json = req.query.c
      .replace(/\[/g, '["')
      .replace(/,/g, '","')
      .replace(/\]/g, '"]');
    var requestedCourses = Bourne.parse(json);
    requestedCourses.isBase64 = true;
  } else if (req.query.courses) {
    // Backwards compatibility of the get parameter vakken.
    var requestedCourses = Bourne.parse(req.query.courses);
    requestedCourses.isBase64 = false;
  } else {
    // More backwards compatibility of the get parameter vakken.
    var requestedCourses = Bourne.parse(req.query.vakken);
    requestedCourses.isBase64 = false;
  }

  getSchedule(function(bodySemester1, bodySemester2) {
    let calendar = generateIcs.loadCalendarInfo(
      bodySemester1,
      bodySemester2,
      requestedCourses
    );

    res.set({
      "Content-Type": "text/calendar",
      "Content-Disposition": "attachment; filename=calendar.ics"
    });
    res.status(200);
    res.send(calendar);
    res.end();
    monitor(req);
  });
});

app.set("view engine", "pug");

app.get("/generator", (req, res) => {
  getSchedule(function(bodySemester1, bodySemester2) {
    let calendarEntries = scraper.scrapeCalendarInfo([
      bodySemester1,
      bodySemester2
    ]);

    res.status(200);
    res.render("generator", { calendarEntries: calendarEntries });
    res.end();
    monitor(req);
  });
});

app.listen(PORT, () => {
  console.log(`Listening on port ${PORT}!`);
});